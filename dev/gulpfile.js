
const { src, dest, watch, series } = require('gulp');

const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const concat= require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

sass.compiler = require('node-sass');

function minifyCSS(){
	return src('css/dev.css')
    .pipe(cleanCSS({
	      compatibility: 'ie8',
	      level: 2
		  }))
	.pipe(autoprefixer())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest('css'));
}

function compileSCSS(){
	return src('scss/Dev/dev.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(dest('css'));
}

exports.default = function() {
	console.log('Compilando SCSS...');
	compileSCSS();
	console.log('Concluído');
	console.log('Minificando CSS...');
	minifyCSS();
	console.log('Concluído');
	watch('scss/Dev/dev.scss', compileSCSS);
	watch('css/dev.css', minifyCSS);
  return true;
} 