
const { src, dest, watch, series } = require('gulp');

const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const concat= require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

sass.compiler = require('node-sass');

function minifyCSS(){
	return src('css/main.css')
    .pipe(cleanCSS({
	      compatibility: 'ie8',
	      level: 2
		  }))
	.pipe(autoprefixer())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest('css'));
}
function minifyCSSSite(){
	return src('css/main_site.css')
    .pipe(cleanCSS({
	      compatibility: 'ie8',
	      level: 2
		  }))
	.pipe(autoprefixer())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest('css'));
}

function compileSCSS(){
	return src('scss/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(dest('css'));
}

exports.default = function() {
	console.log('Compilando SCSS...');
	compileSCSS();
	console.log('Concluído');
	console.log('Minificando CSS...');
	minifyCSS();
	console.log('Concluído');
	watch('scss/**/*.scss', compileSCSS);
	watch('css/main.css', minifyCSS);
	watch('css/main_site.css', minifyCSSSite);
  return true;
} 